class Tab {
  constructor(arrObj) {
    this.tabItems = document.createElement('div');
    this.tabItems.classList.add('tab__items');

    this.tabs = [];
    this.tabindexCnt = 1;
    this.currentTab;


    for (const obj of arrObj) {
      const tab = this.addItem(obj.title, obj.data);

      

      this.tabs.push(tab);
      this.tabItems.appendChild(tab);
    }

  }

  addItem(name, data) {
    let item = document.createElement('div');
    item.classList.add('tab__item');
    item.textContent = name;
    item.id = this.tabindexCnt;
    item.tabIndex = this.tabindexCnt++;
    



    item.addEventListener('click', (event)=>{
      this.currentTab = event.target.id;
      this.pickOut(event.target);
      const info = document.getElementById('info');
      info.innerText = data;
    });


    item.addEventListener('keydown', (event)=>{
      console.log(event.target);
      if(event.code === 'ArrowRight') {
        if(event.target.id < 3) {
          const nextElement = event.target.nextElementSibling;
          nextElement.focus();
          console.log(nextElement);
        }
      }
      else if(event.code === 'ArrowLeft') {
        if(event.target.id > 0) {
          const perviousElement = event.target.previousElementSibling;
          perviousElement.focus();
          console.log(perviousElement);
        }
      }
      else if (event.code === 'Enter') {
        this.pickOut(event.target);
        const info = document.getElementById('info');
        info.innerText = data;
      }
    });


    return item;
  }

  /*Пока так*/
  pickOut(target) {
    this.currentTab = target.id;
    target.blur();
    for (const tab of this.tabs) {
      if(tab.classList.contains('_current')) {
        tab.classList.remove('_current')
      }
    }
    target.classList.add('_current');
  }

  render() {
    return this.tabItems;
  }

  static addContent() {
    const content = document.createElement('div');
    content.id = 'info';

    const pageTab = document.getElementById('tab');
    pageTab.appendChild(content);
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())
Tab.addContent();
